import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

	public static int[][]grid;
	public int GetN() {
		return grid.length;
	}
	 // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
    	int CurrNumber = 1;
    	grid = new int [n][n];
    	for(int i = 0 ; i < n ; i++) {
    		for(int j = 0 ; j < n ; j++) {
    			grid[i][j] = CurrNumber*-1;
    			CurrNumber++;
    		}
    	}
    }

      // opens the site (row, col) if it is not open already
    	public void open(int row, int col) {
    		if(grid[row][col] < 0) grid[row][col] = grid[row][col]*-1;
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
    	if(grid[row][col]>0) return true;
    	return false;
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
    	
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
    	int br = 0;
    	for(int i = 0 ; i < this.GetN()  ; i++) {
    		for(int j = 0 ; i < this.GetN() ; i++) {
    			if(grid[i][j]>0)br++;
    		}
    	}
    	return br;
    }

      // does the system percolate?
//    public boolean percolates();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	Percolation p = new Percolation(4);
    	  System.out.println("The 2D array is: ");
          for (int i = 0; i < 4; i++) {
              for (int j = 0; j < 4; j++) {
                  System.out.print(p.grid[i][j] + " ");
              }
              System.out.println();
          }
          System.out.println(p.GetN());
    }
}
